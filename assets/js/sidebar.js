document.addEventListener("DOMContentLoaded", function () {
  const buttons = document.querySelectorAll(".hextra-sidebar-collapsible-button");
  buttons.forEach(function (button) {
    button.addEventListener("click", function (e) {
      e.preventDefault();
      const list = button.parentElement.parentElement;
      if (list) {
        list.classList.toggle("open")
      }
    });
  });

  // MIT LICENSE Copyright(c) 2022 - 2023 Dumindu Madunuwan
  if ('IntersectionObserver' in window && {{ cond (site.Params.tocHighlightHeading|default true) `true` `false` }}) {
    const links = document.querySelectorAll('aside.sidebar-container ul li a[href^=\\#]');
    let activeLink = null;
    const linksById = {};

    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          if (activeLink) {
            activeLink.classList.remove('active');
          }

          activeLink = linksById[entry.target.id];
          if (activeLink) {
            activeLink.classList.add('active');
          }
        }
      });
    }, { rootMargin: `0% 0% -80% 0%` });

    links.forEach(link => {
      const id = link.getAttribute('href') ? link.getAttribute('href').slice(1) : null; // Checking if href exists before slicing #
      if (id) {
        const target = document.getElementById(id);
        if (target) {
          linksById[id] = link;
          observer.observe(target);
        }
      }
    });
  }


});
