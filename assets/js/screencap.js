document.addEventListener("DOMContentLoaded", function () {
  var screencaps = document.querySelectorAll(".screencap video");
  function playScreencap(screencap, hard) {
    screencap.dataset.state = "play";
    if (hard) {
      screencap.dataset.userstate = "play";
    }
    let progressBar = screencap.nextElementSibling.getAnimations()[0];
    screencap.play();
    progressBar.play();
  }
  function pauseScreencap(screencap, hard) {
    screencap.dataset.state = "pause";
    if (hard) {
      screencap.dataset.userstate = "pause";
    }
    let progressBar = screencap.nextElementSibling.getAnimations()[0];
    screencap.pause();
    progressBar.pause();
  }
  screencaps.forEach(screencap => {
   screencap.addEventListener("loadedmetadata", event => {
      screencap.nextElementSibling.getAnimations()[0].effect.updateTiming({ duration: screencap.duration*1000 });
    })
    screencap.addEventListener("mouseenter", event => {
      if (screencap.dataset.userstate !== "pause") {
        playScreencap(screencap);
      }
    });
    screencap.addEventListener("mouseleave", event => {
      if (screencap.dataset.userstate !== "play") {
        pauseScreencap(screencap);
      }
    });
    screencap.addEventListener("click", event => {
      if (screencap.paused) {
        playScreencap(screencap, true);
      } else {
        pauseScreencap(screencap, true);
      }
      screencap.dataset.on = "false";
    });
    window.addEventListener("blur", event => {
      if (screencap.dataset.userstate !== "pause") {
        playScreencap(screencap);
      };
      screencap.dataset.on = "true";
    });
    window.addEventListener("focus", event => {
      if (screencap.dataset.userstate !== "play") {
        pauseScreencap(screencap);
      };
      screencap.dataset.on = "false";
    });
    document.addEventListener("document", event => {
      if (document.hidden) {
        screencap.dataset.on = "false";
      }
    })
  });
});
