document.addEventListener("DOMContentLoaded", function () {
  var osTabsSwitchAll = {{ cond (site.Params.osTabsSwitchAll |default true) `true` `false` }};
  var currentOS = ""; // = getCustomizedOS(getPredefinedOS());
  function getCustomizedOS(defaultVal = null) {
    console.log("get custom OS");
    var customizedOS = localStorage.getItem("ud-ostabs-os");
    if (customizedOS) {
      return customizedOS;
    }
    return defaultVal;
  }
  function getPredefinedOS(defaultVal = null) {
    console.log("get predefined OS");
    if (navigator.platform.toUpperCase().indexOf("WIN") != -1) return "windows";
    if (navigator.platform.toUpperCase().indexOf("MAC") != -1) return "mac";
    if (navigator.platform.toUpperCase().indexOf("X11") != -1) return "unix";
    if (navigator.platform.toUpperCase().indexOf("LINUX") != -1) return "linux";
    if (navigator.platform.toUpperCase().indexOf("BSD") != -1) return "bsd";
    if (navigator.platform.toUpperCase().indexOf("ANDROID") != -1) return "apps";
    return defaultVal;
  }
  function tabContainersSwitch(containers, newPanelID) {
    console.log("switching tabs");
    containers.forEach(function (container) {
      // Select correct tab
      const tabs = Array.from(container.querySelectorAll('.tabs-toggle'));
      tabs.forEach(function (tab) {
        if (tab.getAttribute('aria-controls') === newPanelID) {
          tab.dataset.state = 'selected';
        } else {
          tab.dataset.state = '';
        }
      })

      // Select correct panel
      const panelsContainer = container.nextElementSibling;
      Array.from(panelsContainer.children).forEach(function (panel) {
        if (panel.id === newPanelID) {
          panel.dataset.state = 'selected';
        } else {
          panel.dataset.state = '';
        }
      });
    });
  }
  function tabsEventHandler(e) {
    console.log("handler called");
    if (!e || e.type === "visibilitychange" && !document.hidden || e.type === "focus") {
      var newOS = getCustomizedOS(getPredefinedOS());
      if (newOS !== currentOS) {
        var containers = Array.from(document.querySelectorAll('.ud-ostabs'));
        tabContainersSwitch(containers, 'tabs-panel-' + newOS);
        currentOS = newOS;
      }
    } else if (e.type === "click") {
      if (e.target.parentElement.classList.contains('ud-ostabs') && osTabsSwitchAll) {
        var predefinedOS = getPredefinedOS();
        var clickedOS = e.target.getAttribute('aria-controls').match(/^tabs-panel-([^\n]+)$/);
        if (clickedOS && clickedOS.length > 0 && clickedOS[1] !== currentOS) {
          var containers = Array.from(document.querySelectorAll('.ud-ostabs'));
          tabContainersSwitch(containers, e.target.getAttribute('aria-controls'));
          currentOS = clickedOS[1];
          if (clickedOS[1] === predefinedOS) {
            localStorage.removeItem("ud-ostabs-os");
          } else {
            localStorage.setItem("ud-ostabs-os", clickedOS[1]);
          }
        }
      } else {
        var containers = Array(e.target.parentElement);
        tabContainersSwitch(containers, e.target.getAttribute('aria-controls'));
      }
    }
  }

  tabsEventHandler();

  if (osTabsSwitchAll) {
    document.addEventListener("visibilitychange", tabsEventHandler);
    window.addEventListener("blur", tabsEventHandler);
    window.addEventListener("focus", tabsEventHandler);
  }
  document.querySelectorAll('.tabs-toggle').forEach(function (button) {
    button.addEventListener('click', tabsEventHandler);
  });
});