module gitlab.com/maxvalue/unidocs/exampleSite

go 1.20

replace github.com/imfing/hextra => ../
replace gitlab.com/maxvalue/unidocs => ../
