---
title: Screencap Component
linkTitle: Screencaptures
aliases:
- screencaps
---

A built-in component to display a screen recording to the user.

<!--more-->

## Example

{{< screencap "video/menu_qgis_settings.webm" >}}

## Usage

{{< screencap "video/menu_qgis_settings.webm" >}}

```
{{/< screencap "video/menu_qgis_settings.webm" >/}}
```

You can also have the screencap besides some content. In this instances the screencap is floated to the right. {{< screencap "video/menu_qgis_settings.webm" "right" >}}

```
{{/< screencap "video/menu_qgis_settings.webm" "right" >/}}
```
