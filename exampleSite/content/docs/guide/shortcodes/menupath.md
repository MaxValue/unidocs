---
title: Menupath Component
linkTitle: Menupaths
aliases:
- menupath
---

A built-in component to display a path in a program nicely to the user.

<!--more-->

## Example

{{< menupath "View/Data/CSV" >}}

## Usage

{{< menupath "View/Data/CSV" >}}

```
{{/< menupath "View/Data/CSV" >/}}
```
